/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraysandloops;

/**
 *
 * @author lmangoua
 */
public class ArraysAndLoops {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int[] lotteryNumbers = new int[49];
        
        for(int i = 0; i < lotteryNumbers.length; i++) {
            lotteryNumbers[i] = i + 1;
            System.out.println("Lottery numbers are:");
            System.out.println(lotteryNumbers[i]);
        }
        
    }
    
}
