/*
 *If you don't know how many items are going to be held in your array, 
 *you may be better off using something called an ArrayList. 
 *An ArrayList is a dynamic data structure, meaning items can be added 
 *and removed from the list. A normal array in java is a static data structure, 
 *because you stuck with the initial size of your array.
 */
package arraylists;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author lmangoua
 * Source: http://www.homeandlearn.co.uk/java/array_lists.html
 */
public class ArrayLists {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList listTest = new ArrayList();

        listTest.add("first item");
        listTest.add("second item");
        listTest.add("third item");
        listTest.add(7);

        Iterator it = listTest.iterator();

        while (it.hasNext()) {
            System.out.println(it.next());
        }

        //Remove an item from the list
        listTest.remove("second item");

        //Print out the new list
        System.out.println("Whole list = " + listTest);

        //Get the item at index position 2
        System.out.println("Position 2 = " + listTest.get(2)); //This line will get the item at Index position 3 on the list. Index numbers start counting at zero, so this will be the third item.

    }

}
