/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helloworld;

import inputoutput.UserInput;
import java.util.Scanner;

/**
 *
 * @author lmangoua
 */
public class HelloWorld {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
//        //<editor-fold defaultstate="collapsed" desc="Numbers printing project">
//        /**
//         * We can use the numbers from the array set in the code as below commented out
//         */
//        
////        int numbers[] = {10, 15, 2, 50, 43};
//
///**
// * Or we can just set the number of elements we want in our array, then enter manually these numbers from the console as done there
// */
//
//Scanner sc = new Scanner(System.in);
//
//System.out.println("Enter number of elements");
//
//int n = sc.nextInt();
//int numbers[] = new int[n];
//
//System.out.println("Enter numbers");
//
//for (int i = 0; i < numbers.length; i++) {
//    numbers[i] = sc.nextInt();
//}
//
//PrintAll(numbers);
//Largest(numbers);
//Smallest(numbers);
//Descending(numbers);
//Ascending(numbers);
////</editor-fold>

        UserInput.usingScanner();
        UserInput.usingJOption();
        
    }

    public static void PrintAll(int[] num) {
        System.out.println("Print All numbers");
        
        for (int i = 0; i < num.length; i++) {
            System.out.print(num[i] + " ");            
        }
        System.out.println();
        System.out.println("***********");
    }
    
    public static void Largest(int[] num) {
        System.out.println("Print the Largest number");
        
        int large = num[0];
        for (int i = 0; i < num.length; i++) {
            if (num[i] > large) {
                large = num[i];
            }
        }
        System.out.println("The Largest number is: " + large);
        System.out.println("***********");
    }
    
    public static void Smallest(int[] num) {
        System.out.println("Print the Smallest number");
        
        int small = num[0];
        for (int i = 0; i < num.length; i++) {
            if (num[i] < small) {
                small = num[i];
            }
        }
        System.out.println("The Smallest number is: " + small);
        System.out.println("***********");
    }
    
    public static void Descending(int[] desc) {
        System.out.println("Print numbers in Descending order");
        
        int i, j, temp;
        
        for (i = 0; i < desc.length; i++) {
            for (j = i + 1; j < desc.length; j++) {
                if (desc[i] < desc[j]) {
                    temp = desc[i];
                    desc[i] = desc[j];
                    desc[j] = temp;
                }
            }
        }
        
        for (i = 0; i < desc.length; i++) {
            System.out.print(desc[i] + " ");
        }
        System.out.println();
        System.out.println("***********");
    }
    
    public static void Ascending(int[] asc) {
        System.out.println("Print numbers in Ascending order");
        
        int i, j, temp;
        
        for (i = 0; i < asc.length; i++) {
            for (j = i + 1; j < asc.length; j++) {
                if (asc[i] > asc[j]) {
                    temp = asc[i];
                    asc[i] = asc[j];
                    asc[j] = temp;
                }
            }
        }
        
        for (i = 0; i < asc.length; i++) {
            System.out.print(asc[i] + " ");
        }
        System.out.println();
        System.out.println("***********");
    }


}
