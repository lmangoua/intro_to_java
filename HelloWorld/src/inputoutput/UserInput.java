/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inputoutput;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author lmangoua
 * Date: 08/09/2016
 */
public class UserInput {
    
    public static void usingScanner() {
        
        Scanner userInput = new Scanner(System.in);
        String name, surname;
        
        System.out.print("Enter your Name: ");
        name = userInput.next();
        
        System.out.print("Enter your Surname: ");
        surname = userInput.next();
        
        System.out.println("Your full name is: " + name + " " + surname);
        
    }
    
    public static void usingJOption() {
        
        String name, surname;
        String fullName;
        
        name = JOptionPane.showInputDialog("Enter your Name: ");
        
        surname = JOptionPane.showInputDialog("Enter your SurName: ");
                
        fullName = "Your full name is: " + name + " " + surname;
        
        JOptionPane.showMessageDialog(null, fullName);
        System.exit(0);
        
    }
    
}
