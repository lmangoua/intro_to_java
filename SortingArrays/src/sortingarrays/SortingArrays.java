/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sortingarrays;

import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author lmangoua
 */
public class SortingArrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int[] aryNums = new int[6];
        
        aryNums[0] = 10;
        aryNums[1] = 16;
        aryNums[2] = 14;
        aryNums[3] = 20;
        aryNums[4] = 9;
        aryNums[5] = 30;
        
        //Sort ascending array
        Arrays.sort(aryNums);
        
        System.out.println("Sort ascending array: ");
        for(int i = 0; i < aryNums.length; i++) {
            System.out.println("Num: " + aryNums[i]);
        }
        
        //Sort descending array
        Integer[] integerArray = new Integer[aryNums.length];
        
        for(int i = 0; i < aryNums.length; i++) {
            integerArray[i] = new Integer(aryNums[i]);
        }
        
        Arrays.sort(integerArray, Collections.reverseOrder());
        
        System.out.println("*********************************");
        System.out.println("Sort descending array: ");
        for(int i = 0; i < aryNums.length; i++) {
            System.out.println("Num: " + integerArray[i]);
        }
        
    }
}
